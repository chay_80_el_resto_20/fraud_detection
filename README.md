## Fraudes

El objetivo de este proyecto es mostrar mas o menos un caso end to end. La parte correspondiente a el estudio de la base de datos y la generacion de features a partir de las fuentes no se toca ya que kaggle provee solo un dataset donde todo ya esta hecho. El resto si lo podemos hacer.

Este miniprojecto puede ser dividido en varias partes que no necesariamente son  secuenciales. Cada una de estas partes es un proyecto corto en si y puede dar origen al menos a articulos en internet

* Creacion de modelos explicativos basado en entendimiento del problema y generacion de resultados accionables
* Dashboard para mostrar los resultados del modelo, considerando los diferentes roles envueltos.
* Deployment del modelo en sistemas de la empresa
* Deployment utilizando big data
* Deployment big data + real time
