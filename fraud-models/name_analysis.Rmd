---
title: "R Notebook"
output: html_notebook
---

```{r}
pmts_df = fread(file.path("..","data","payments.csv"))%>%
  type_convert()

#Typecasting
pmts_df = pmts_df %>%
  mutate_at(.vars = vars(type,isFraud,isFlaggedFraud), as.factor)
```

```{r}
pmts_df%>%
  count(isFraud)
```


```{r}
nombres = pmts_df%>%
  select(nameOrig, nameDest, isFraud, type, step)

involving_fraude = inner_join(nombres, nombres, by=c("nameDest"="nameOrig"))%>%
  filter(step.y >= step.x)

head(involving_fraude)

involving_fraude%>%
  #filter(isFraud.x=="1")%>%
  count(isFraud.x,isFraud.y)

involving_fraude%>%
  filter(isFraud.x=="1")%>%
  count(isFraud.y, type.x, type.y)
```

```{r}
nombres_combinados = tibble(nombre = c(nombres$nameOrig, nombres$nameDest))

nombres_combinados%>%
  count(nombre)%>%
  filter(n>1)
```

